package com.example.demo.Person;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(path = "person") //et ei oleks lihtsalt 8080
public class PersonController {


    private final PersonRepo personRepo;

    @Autowired
    public PersonController(PersonRepo personRepo) {
        this.personRepo = personRepo;
    }

    @GetMapping("/all")
    public ResponseEntity<List<Person>> getPeople(){
        List<Person> people =  personRepo.findAll();
        return new ResponseEntity<>(people, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Optional<Person>> getPersonById(@PathVariable("id") Long id){
        Optional<Person> person =  personRepo.findById(id);
        return new ResponseEntity<>(person, HttpStatus.OK);
    }

    @PostMapping("/add")
    ResponseEntity<Person> addPerson(@RequestBody Person person){
        Person newPerson = personRepo.save(person);
        return new ResponseEntity<>(newPerson, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Person> UpdatePersonById(@PathVariable("id") Long id, @RequestBody Person person){
        Person oldperson =  personRepo.findById(id).orElseThrow(() ->
                new RuntimeException("Person with id "+ id + " could not be found"));

        oldperson.setFirstname(person.getFirstname());
        oldperson.setLastname(person.getLastname());
        oldperson.setGender(person.getGender());
        oldperson.setBirthday(person.getBirthday());

        Person newperson = personRepo.save(oldperson);

        return new ResponseEntity<>(newperson, HttpStatus.OK);
    }


}
