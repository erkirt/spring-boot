package com.example.demo.Person;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import java.util.List;

@SpringBootApplication
public class PersonConfig {

    public static void main(String[] args) {
        SpringApplication.run(PersonConfig.class, args);
    }


    @Bean
    public CorsFilter corsFilter() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowCredentials(true);
        configuration.setAllowedOrigins(List.of("http://localhost:4200"));
        configuration.setAllowedHeaders(List.of("Origins", "Access-Control-Allow-Origin", "Content-Type", "Accept",
                "Authorization", "Origin, Accept", "X-Requested-With", "Access-Control-Request-Method",
                "Access-Control-Request-Headers"));
        configuration.setExposedHeaders(List.of("Origins", "Content-Type", "Accept",
                "Authorization", "Access-Control-Allow-Origin", "Access-Control-Allow-Credentials"));
        configuration.setAllowedMethods(List.of("GET", "POST", "PUT", "DELETE", "OPTIONS"));

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return new CorsFilter(source);
    }

    @Bean
    CommandLineRunner commandLineRunner(PersonRepo repo){

        //ei lase ilma id-ta buildida

        return args -> {
            Person ergo = new Person(
                    null,
                    "Ergo",
                    "Kirt",
                    "Mees",
                    "22/04/2000"
            );

            Person vega = new Person(
                    null,
                    "Agathe",
                    "Vega",
                    "Naine",
                    "07/03/2002"
            );
            Person jaanus = new Person(
                    null,
                    "Jaanus",
                    "Peetrus",
                    "Mees",
                    "02/02/2000"
            );
            repo.saveAll(List.of(ergo, vega, jaanus));
            repo.findAll().forEach(System.out::println);
    };
    }
}
